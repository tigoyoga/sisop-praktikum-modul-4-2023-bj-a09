#define FUSE_USE_VERSION 28
#include <dirent.h>
#include <sys/time.h>
#include <sys/xattr.h>
#include <stdbool.h>
#include <unistd.h>
#include <fuse.h>
#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <fcntl.h>
#include <string.h>

#define MAX_INPUT_SIZE 1024

static const char *logpath = "/home/tigoyoga/fs_module.log";
static const char *dirpath = "/home/tigoyoga";

void modular(char *path){ 
	DIR *dir = opendir(path);  
	struct dirent *entry;
	struct stat sb;

	while ((entry = readdir(dir))){ // Looping untuk membaca file
    	if (!strcmp(entry->d_name, ".") || !strcmp(entry->d_name, "..")) continue;  // Skip file . dan ..

    	char curr[300]; // Current path
    	sprintf(curr, "%s/%s", path, entry->d_name);    // Concatenate path
    	if (stat(curr, &sb) == -1) continue;    // Get file status

    	if (S_ISDIR(sb.st_mode)){   // Jika direktori maka rekursif 
        	modular(curr);
    	}
    	else if (S_ISREG(sb.st_mode)){  // Jika file reguler maka modular
        	FILE *input = fopen(curr, "rb");    // Open file
        	if (input == NULL) return;  // Jika file tidak ada maka return

        	fseek(input, 0, SEEK_END);  // Get file size
        	long fileSize = ftell(input);   // Get file size
        	if (fileSize <= MAX_INPUT_SIZE) continue;   // Jika file size <= 1024 maka continue
        	rewind(input);  // Reset file pointer

        	int count = (fileSize + MAX_INPUT_SIZE - 1) / MAX_INPUT_SIZE;   // Get chunk count

        	for (int i = 0; i < count; i++){    // Looping untuk membagi file
            	char chunkNum[5], chunkPath[350];   // Chunk number dan chunk path
            	if (i < 10) sprintf(chunkNum, "00%d", i);   // Jika chunk number < 10 maka 00%d
            	else if (i < 100) sprintf(chunkNum, "0%d", i);  // Jika chunk number < 100 maka 0%d
            	else sprintf(chunkNum, "%d", i);    // Jika chunk number > 100 maka %d
            	sprintf(chunkPath, "%s.%s", curr, chunkNum);    // Concatenate chunk path

            	FILE *output = fopen(chunkPath, "wb");  // Open chunk path untuk write
            	if (output == NULL) return;

            	char *buffer = (char*) malloc(MAX_INPUT_SIZE);  // Buffer untuk membaca file
            	if (buffer == NULL) return;

            	size_t bytes = fread(buffer, 1, MAX_INPUT_SIZE, input); // Read file
            	fwrite(buffer, 1, bytes, output);   // Write file
            	free(buffer);
            	fclose(output);
        	}
        	fclose(input);

        	char command[700];  // Command untuk menghapus file
        	sprintf(command, "rm %s ", curr);   // Concatenate command
        	system(command);
    	}
	}
	closedir(dir);
}

void merge(const char *path){       // Fungsi untuk menggabungkan file
    DIR *dir = opendir(path);    // Open directory
    struct dirent *entry;               
    struct stat sb;             // File status  
    while ((entry = readdir(dir))){    // Looping untuk membaca file
        if (!strcmp(entry->d_name, ".") || !strcmp(entry->d_name, "..")) continue;  // Skip file . dan ..
        char curr[300];
        snprintf(curr, sizeof(curr), "%s/%s", path, entry->d_name); // Concatenate path
        if (stat(curr, &sb) == -1) continue;    // Get file status
        if (S_ISDIR(sb.st_mode)){   // Jika direktori maka rekursif
            merge(curr);
        }
        else if (S_ISREG(sb.st_mode) && strlen(curr) > 3 && !strcmp(curr + strlen(curr) - 3, "000")){   // Jika file reguler dan file terakhir maka merge file
            int count = 0;  // Chunk count
            char dest[300], temp[350];  // Destination path dan temporary path
            memset(dest, '\0', sizeof(dest));   // Clear destination path
            memset(temp, '\0', sizeof(temp));   // Clear temporary path
            strncpy(dest, curr, strlen(curr) - 4);  // Copy destination path
            while(1){
                if (count < 10) snprintf(temp, sizeof(temp), "%s.00%d", dest, count);   // Concatenate temporary path
                else if (count < 100) snprintf(temp, sizeof(temp), "%s.0%d", dest, count);  // Concatenate temporary path
                else snprintf(temp, sizeof(temp), "%s.%d", dest, count);    // Concatenate temporary path

                if (stat(temp, &sb)) break;
                FILE *destfile = fopen(dest, "ab"); // Open destination file
                FILE *tempfile = fopen(temp, "rb"); // Open temporary file
                if (destfile && tempfile) { // Jika file ada maka read dan write
                    char buffer[1024];
                    size_t read_bytes;
                    while ((read_bytes = fread(buffer, 1, sizeof(buffer), tempfile)) > 0) { // Read file
                        fwrite(buffer, 1, read_bytes, destfile);    // Write file
                    }   
                }
                if (tempfile) fclose(tempfile); // Close file
                if (destfile) fclose(destfile); // Close file
                remove(temp);
                count++;
            }
        }
    }
    closedir(dir);
}

void create_log(const char *type, const char *call, const char *arg1, const char *arg2){  // Fungsi untuk menulis log
    time_t t = time(NULL);  // Get current time
    struct tm *curr = localtime(&t);    // Get current time
    char logmsg[1000];  // Log message
    if (arg2 == NULL){  // Jika argumen kedua kosong maka
        snprintf(logmsg, sizeof(logmsg), "%s::%02d%02d%02d-%02d:%02d:%02d::%s::%s", type, (curr->tm_year + 1900) % 100, curr->tm_mon + 1, curr->tm_mday, curr->tm_hour, curr->tm_min, curr->tm_sec, call, arg1);
    }
    else {  // Jika argumen kedua tidak kosong maka
        snprintf(logmsg, sizeof(logmsg), "%s::%02d%02d%02d-%02d:%02d:%02d::%s::%s::%s", type, (curr->tm_year + 1900) % 100, curr->tm_mon + 1, curr->tm_mday, curr->tm_hour, curr->tm_min, curr->tm_sec, call, arg1, arg2);
    }
    FILE *logfile = fopen(logpath, "a");    // Open log file
    if (logfile != NULL) {  // Jika file ada maka tulis log
        fprintf(logfile, "%s\n", logmsg);   // Write log
        fclose(logfile);
    }
}

static int do_create(const char *path, mode_t mode, struct fuse_file_info *fi){ // Fungsi untuk membuat file
	char fpath[1000];   // File path
	sprintf(fpath, "%s%s", dirpath, path);  // Concatenate file path
	int res = open(fpath, fi->flags, mode); // Open file
	create_log("REPORT", "CREATE", (char*) path, (res == -1) ? strerror(errno) : NULL); // Tulis log
	return (res == -1) ? -errno : res;  // Return
}

static int do_release(const char *path, struct fuse_file_info *fi){ // Fungsi untuk melepaskan file
    create_log("REPORT", "RELEASE", (char*) path, NULL);    // Tulis log
    return 0;
}

static int do_getattr(const char *path, struct stat *stbuf){    // Fungsi untuk mendapatkan atribut file
    char fpath[1000];   // File path
    sprintf(fpath, "%s%s", dirpath, path);  // Concatenate file path
    int res = lstat(fpath, stbuf);  // Get file status
    create_log("REPORT", "GETATTR", (char*) path, (res == -1) ? strerror(errno) : NULL);    // Tulis log
    return (res == -1) ? -errno : 0;
}

static  int  do_readlink(const char *path, char *buf, size_t size){ // Fungsi untuk membaca link
	char fpath[1000];   // File path
	sprintf(fpath, "%s%s", dirpath, path);  // Concatenate file path
	int res = readlink(fpath, buf, size - 1);   // Read link
	create_log("REPORT", "READLINK", (char*) path, (res == -1) ? strerror(errno) : NULL);   // Tulis log
	return (res == -1) ? -errno : 0;    // Return
}

static int do_access(const char *path, int mode){   // Fungsi untuk mengakses file
    char fpath[1000];   // File path
    sprintf(fpath, "%s%s", dirpath, path);  // Concatenate file path
    int res = access(fpath, mode);  // Access file
    create_log("REPORT", "ACCESS", (char*) path, (res == -1) ? strerror(errno) : NULL);  // Tulis log
    return (res == -1) ? -errno : 0;
}

static int do_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi){    // Fungsi untuk membaca direktori
	char fpath[1000];   // File path
	sprintf(fpath, "%s%s", dirpath, path);  // Concatenate file path
	DIR *dp;    // Directory pointer
	struct dirent *de; // Dirent pointer
	(void) fi;  // File info pointer
	(void) offset; // Offset pointer 
	dp = opendir(fpath);    // Open directory
	if (dp == NULL) return -errno; // Jika directory tidak ada maka return
	while ((de = readdir(dp))) {    // Looping untuk membaca file
    	struct stat st; // File status
    	memset(&st, 0, sizeof(st));   // Clear file status
    	st.st_ino = de->d_ino;  // File inode
    	st.st_mode = de->d_type << 12;  // File mode
    	if ((filler(buf, de->d_name, &st, 0)) != 0) break;  // Fill buffer
	}
	closedir(dp);   // Close directory
	create_log("REPORT", "READDIR", (char*) path, NULL);    // Tulis log

	return 0;
}

static int do_mknod(const char *path, mode_t mode, dev_t dev){  // Fungsi untuk membuat node
	char fpath[1000];   // File path
	sprintf(fpath, "%s%s", dirpath, path);  // Concatenate file path
	int res = mknod(fpath, mode, dev);  // Membuat node
	create_log("REPORT", "MKNOD", (char*) path, (res == -1) ? strerror(errno) : NULL);  // Tulis log
	return (res == -1) ? -errno : 0;    // Return 
}

static int do_link(const char *from, const char *to){   // Fungsi untuk membuat link
	char from_path[1000], to_path[1000];  // From path dan to path
	sprintf(from_path, "%s%s", dirpath, from);   // Concatenate from path
	sprintf(to_path, "%s%s", dirpath, to);   // Concatenate to path
	int res = link(from_path, to_path);   // Membuat link
    if (res == -1) return -errno;   // Jika link tidak ada maka return
	create_log("REPORT", "LINK", (char*) from, (char*) to);  // Tulis log
	return 0;
}

static int do_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi){  // Fungsi untuk membaca file
    char fpath[1000];   // File path
    sprintf(fpath, "%s%s", dirpath, path);  // Concatenate file path

    int fd = open(fpath, O_RDONLY); // Open file
    if (fd == -1) return -errno;    // Jika file tidak ada maka return

    int res = pread(fd, buf, size, offset); // Read file
    if (res == -1) res = -errno;    // Jika file tidak ada maka return

    close(fd);
    create_log("REPORT", "READ", (char*) path, NULL);   // Tulis log
    return res;
}

static int do_rename(const char *old, const char *new){  // Fungsi untuk mengganti nama file
	char oldpath[1000], newpath[1000];  // Old path dan new path
	sprintf(oldpath, "%s%s", dirpath, old);  // Concatenate old path
	sprintf(newpath, "%s%s", dirpath, new); // Concatenate new path
	int res;    // Result
	res = rename(oldpath, newpath); // Rename file
	if (res == -1) return -errno;   // Jika file tidak ada maka return    
	create_log("REPORT", "RENAME", (char*) old, (char*) new);   // Tulis log

	char curr[1000], dup[1000]; // Current path dan duplicate path
	strcpy(curr, dirpath);  // Copy current path
	strcpy(dup, new);   // Copy duplicate path
	char *prefix, *oldname = strrchr(old, '/'), *newname = strrchr(new, '/');   // Prefix, old name, dan new name  
	prefix = strtok(dup, "/");  // modular duplicate path

	while(prefix != NULL){  // Looping untuk membagi file
    	strcat(curr, "/");  // Concatenate current path
    	strcat(curr, prefix);   // Concatenate current path
    	struct stat sb; // File status
    	if (stat(curr, &sb) == -1) continue;    // Get file status
    	if (strlen(prefix) >= 7 && !strncmp(prefix, "module_", 7) && S_ISDIR(sb.st_mode)){  // Jika direktori dan nama file module_ maka modular
        	modular(curr);    // modular file
        	break;
    	}
    	prefix = strtok(NULL, "/"); // modular duplicate path
	}

	if (strcmp(oldname, newname)){  // Jika old name dan new name tidak sama maka merge
    	struct stat sb;         
    	if ((stat(newpath, &sb) != -1) && S_ISDIR(sb.st_mode) && (strlen(newname) < 8 || strncmp(newname, "/module_", 8))) merge(newpath);  // Jika direktori dan nama file bukan module_ maka merge file
	}

	return 0;
}

static int do_unlink(const char *path){ // Fungsi untuk menghapus link
	char fpath[1000];   // File path
	sprintf(fpath, "%s%s", dirpath, path);  // Concatenate file path
	int res = unlink(fpath);    // Unlink file
    create_log("FLAG", "UNLINK", (char*) path, (res == -1) ? strerror(errno) : NULL);   // Tulis log
	return (res == -1) ? -errno : 0;    // Return
}

static int do_rmdir(const char *path){  // Fungsi untuk menghapus direktori
	char fpath[1000];   // File path
	sprintf(fpath, "%s%s", dirpath, path);  // Concatenate file path
	int res = rmdir(fpath); // Remove direktori
	create_log("FLAG", "RMDIR", (char*) path, (res == -1) ? strerror(errno) : NULL);    // Tulis log
	return (res == -1) ? -errno : 0;    // Return
}

static int do_mkdir(const char *path, mode_t mode){ // Fungsi untuk membuat direktori
	char fpath[1000];   // File path
	sprintf(fpath, "%s%s", dirpath, path);      // Concatenate file path
	int res = mkdir(fpath, mode);   // Membuat direktori
	create_log("REPORT", "MKNOD", (char*) path, (res == -1) ? strerror(errno) : NULL);  // Tulis log
	return (res == -1) ? -errno : 0;    // Return
}

static int do_symlink(const char *to, const char *from){    // Fungsi untuk membuat symlink
    char to_path[1000], from_path[1000];  // To path dan from path
    sprintf(to_path, "%s%s", dirpath, to);   // Concatenate to path  
    sprintf(from_path, "%s%s", dirpath, from);   // Concatenate from path
    int res = symlink(to_path, from_path);    // Membuat symlink
    return (res == -1) ? -errno : (create_log("REPORT", "SYMLINK", (char*) to, (char*) from), 0);   // Tulis log dan return
}

static int do_truncate(const char *path, off_t size){   // Fungsi untuk memotong file   
	char fpath[1000];   // File path
	sprintf(fpath, "%s%s", dirpath, path);  // Concatenate file path
	int res = truncate(fpath, size);    // Memotong file
	create_log("REPORT", "TRUNCATE", (char*) path, (res == -1) ? strerror(errno) : NULL);   // Tulis log
	return (res == -1) ? -errno : 0;
}

static int do_open(const char *path, struct fuse_file_info *fi){    // Fungsi untuk membuka file
	char fpath[1000];   // File path
	sprintf(fpath, "%s%s", dirpath, path);  // Concatenate file path
    int res = open(fpath, fi->flags);   // Open file
    if (res == -1) return -errno;   // Jika file tidak ada maka return
    close(res); // Close file
	create_log("REPORT", "OPEN", (char*) path, NULL);   // Tulis log
    return 0;
}

static int do_chown(const char *path, uid_t uid, gid_t gid){    // Fungsi untuk mengganti kepemilikan file
	char fpath[1000];   // File path
	sprintf(fpath, "%s%s", dirpath, path);  // Concatenate file path
	int res = chown(fpath, uid, gid);   // Mengganti kepemilikan file
	create_log("REPORT", "CHOWN", (char*) path, (res == -1) ? strerror(errno) : NULL);  // Tulis log
	return (res == -1) ? -errno : 0;
}

static int do_write(const char *path, const char *buf, size_t size, off_t offset, struct fuse_file_info *fi){   // Fungsi untuk menulis file
    char fpath[1000];   // File path
    sprintf(fpath, "%s%s", dirpath, path);  // Concatenate file path

    int fd = open(fpath, O_WRONLY); // Open file
    if (fd == -1) return -errno;    // Jika file tidak ada maka return

    int res = pwrite(fd, buf, size, offset);    // Write file
    if (res == -1) res = -errno;        

    close(fd);
    create_log("REPORT", "WRITE", (char*) path, NULL);  // Tulis log
    return res;
}

static int do_fsync(const char *path, int isdatasync, struct fuse_file_info *fi){   // Fungsi untuk melakukan fsync
    create_log("REPORT", "FSYNC", (char*) path, NULL);  // Tulis log
    return 0;
}

static int do_chmod(const char *path, mode_t mode){  // Fungsi untuk mengganti mode file
	char fpath[1000];   // File path
	sprintf(fpath, "%s%s", dirpath, path);  // Concatenate file path
	int res = chmod(fpath, mode);   // Mengganti mode file
	create_log("REPORT", "CHMOD", (char*) path, (res == -1) ? strerror(errno) : NULL);  // Tulis log
	return (res == -1) ? -errno : 0;    // Return
}

static int do_utimens(const char *path, const struct timespec ts[2]){   // Fungsi untuk mengganti waktu akses dan modifikasi file
	char fpath[1000];   // File path
	sprintf(fpath, "%s%s", dirpath, path);  // Concatenate file path
	struct timeval tv[2];   // Time value
	tv[0].tv_sec = ts[0].tv_sec;    // Time value
	tv[0].tv_usec = ts[0].tv_nsec / 1000;   // Time value
    tv[1].tv_sec = ts[1].tv_sec;    // Time value
    tv[1].tv_usec = ts[1].tv_nsec / 1000;   // Time value
	int res = utimes(fpath, tv);    // Mengganti waktu akses dan modifikasi file
	create_log("REPORT", "UTIMENS", (char*) path, (res == -1) ? strerror(errno) : NULL);    // Tulis log
	return (res == -1) ? -errno : 0;
}

static int do_getxattr(const char *path, const char *name, char *value,size_t size){    // Fungsi untuk mendapatkan xattr
	char fpath[1000];   // File path
	sprintf(fpath, "%s%s", dirpath, path);  // Concatenate file path
    int res = lgetxattr(fpath, name, value, size);  // Mendapatkan xattr
    create_log("REPORT", "GETXATTR", (char*) path, (res == -1) ? strerror(errno) : NULL);   // Tulis log
	return (res == -1) ? -errno : res;
}

static int do_removexattr(const char *path, const char *name){  // Fungsi untuk menghapus xattr
	char fpath[1000];   // File path
	sprintf(fpath, "%s%s", dirpath, path);  // Concatenate file path
    int res = lremovexattr(fpath, name);    // Menghapus xattr
    create_log("REPORT", "REMOVEXATTR", (char*) path, (res == -1) ? strerror(errno) : NULL);    // Tulis log
	return (res == -1) ? -errno : 0;
}

static int do_listxattr(const char *path, char *list, size_t size){   // Fungsi untuk mendapatkan list xattr
	char fpath[1000];   // File path
	sprintf(fpath, "%s%s", dirpath, path);  // Concatenate file path
    int res = llistxattr(fpath, list, size);    // Mendapatkan list xattr
    create_log("REPORT", "LISTXATTR", (char*) path, (res == -1) ? strerror(errno) : NULL);  // Tulis log
	return (res == -1) ? -errno : res;
}

static int do_setxattr(const char *path, const char *name, const char *value, size_t size, int flags){  // Fungsi untuk mengatur xattr
	char fpath[1000];   // File path
	sprintf(fpath, "%s%s", dirpath, path);  // Concatenate file path
    int res = lsetxattr(fpath, name, value, size, flags);   // Mengatur xattr
    create_log("REPORT", "SETXATTR", (char*) path, (res == -1) ? strerror(errno) : NULL);   // Tulis log
	return (res == -1) ? -errno : 0;
}

static int do_statfs(const char *path, struct statvfs *stbuf){  // Fungsi untuk mendapatkan statfs
	char fpath[1000];   // File path
	sprintf(fpath, "%s%s", dirpath, path);  // Concatenate file path
    int res = statvfs(fpath, stbuf);    // Mendapatkan statfs
    create_log("REPORT", "STATFS", (char*) path, (res == -1) ? strerror(errno) : NULL); // Tulis log
	return (res == -1) ? -errno : 0;
}


static  struct fuse_operations do_oper = {
	.getattr = do_getattr,
	.access = do_access,
	.readlink = do_readlink,
	.readdir = do_readdir,
	.mknod = do_mknod,
	.mkdir = do_mkdir,
	.symlink = do_symlink,
	.unlink = do_unlink,
	.rmdir = do_rmdir,
	.rename = do_rename,
	.link = do_link,
	.chmod = do_chmod,
	.chown = do_chown,
	.truncate = do_truncate,
	.utimens = do_utimens,
	.open = do_open,
	.read = do_read,
	.write = do_write,
	.statfs = do_statfs,
	.create = do_create,
	.release = do_release,
	.fsync = do_fsync,
	.setxattr = do_setxattr,
	.getxattr = do_getxattr,
	.listxattr = do_listxattr,
	.removexattr = do_removexattr,
};

int main(int  argc, char *argv[]){
	umask(0);
	return fuse_main(argc, argv, &do_oper, NULL);
}

