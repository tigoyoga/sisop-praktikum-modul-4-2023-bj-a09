# sisop-praktikum-modul-4-2023-BJ-A09

Perkenalkan kami dari kelas sistem operasi kelompok A09, dengan anggota sebagai berikut:


|Nama                   |NRP        |
|---|---|
|Muhammad Rifqi Fadhilah|5025211228 |
|Muhammad Naufal Baihaqi|5025211103 |
|Tigo S Yoga            |5025211125 |


# Nomor 1

Kota Manchester sedang dilanda berita bahwa kota ini mau juara (yang kota ‘loh ya, bukan yang satunya). Tagline #YBBA (#YangBiruBiruAja #anjaykelaspepnihbossenggoldong 💪🤖🤙🔵⚪) sudah mewabah di seluruh dunia. Semua warga pun sudah menyiapkan pesta besar-besaran. 
Seorang pelatih sepak bola handal bernama Peb merupakan pelatih klub Manchester Blue, sedang berjuang memenangkan Treble Winner. Untuk meraihnya, ia perlu melakukan pembelian pemain dengan ideal. Agar sukses, ia memahami setiap detail data performa pemain sepak bola seluruh dunia yang meliputi statistik pemain, umur, tinggi dan berat badan, potensi, klub dan negaranya, serta banyak data lainnya. Namun, tantangan tersendiri muncul ketika mengelola dan mengakses data berukuran besar ini.
Kesulitan Peb tersebut mencapai telinga kalian, seorang mahasiswa Teknik Informatika yang ahli dalam pengolahan data. Mengetahui tantangan Peb, kalian diminta untuk membantu menyelesaikan masalahnya melalui beberapa langkah berikut.

## a. 
Langkah pertama, adalah memperoleh data yang akan digunakan. Kalian membuat file bernama storage.c. Oleh karena itu, download dataset tentang pemain sepak bola dari Kaggle. Dataset ini berisi informasi tentang pemain sepak bola di seluruh dunia, termasuk Manchester Blue. Kalian tahu bahwa dataset ini akan sangat berguna bagi Peb. Gunakan command ini untuk men-download dataset.

kaggle datasets download -d bryanb/fifa-player-stats-database

Setelah berhasil men-download dalam format .zip, langkah selanjutnya adalah mengekstrak file tersebut.  Kalian melakukannya di dalam file storage.c untuk semua pengerjaannya. 

## b
Selanjutnya, Peb meminta kalian untuk melakukan analisis awal pada data tersebut dan mencari pemain berpotensi tinggi untuk direkrut. Oleh karena itu, kalian perlu membaca file CSV khusus bernama FIFA23_official_data.csv dan mencetak data pemain yang berusia di bawah 25 tahun, memiliki potensi di atas 85, dan bermain di klub lain selain Manchester City. Informasi yang dicetak mencakup nama pemain, klub tempat mereka bermain, umur, potensi, URL foto mereka, dan data lainnya. kalian melakukannya di dalam file storage.c untuk analisis ini.

## c
Peb menyadari bahwa sistem kalian sangat berguna dan ingin sistem ini bisa diakses oleh asisten pelatih lainnya. Oleh karena itu, kalian perlu menjadikan sistem yang dibuat ke sebuah Docker Container agar mudah di-distribute dan dijalankan di lingkungan lain tanpa perlu setup environment dari awal. Buatlah Dockerfile yang berisi semua langkah yang diperlukan untuk setup environment dan menentukan bagaimana aplikasi harus dijalankan. Setelah Dockerfile berhasil dibuat, langkah selanjutnya adalah membuat Docker Image. Gunakan Docker CLI untuk mem-build image dari Dockerfile kalian. Setelah berhasil membuat image, verifikasi bahwa image tersebut berfungsi seperti yang diharapkan dengan menjalankan Docker Container dan memeriksa output-nya.

## d

Setelah sukses membuat sistem berbasis Docker, Peb merasa bahwa sistem ini tidak hanya berguna untuk dirinya sendiri, tetapi juga akan akan membantu para scouting-nya yang terpencar di seluruh dunia dalam merekrut pemain berpotensi tinggi. Namun, satu tantangan muncul, yaitu bagaimana caranya para scout dapat mengakses dan menggunakan sistem yang telah diciptakan?
Merasa terpanggil untuk membantu Peb lebih jauh, kalian memutuskan untuk mem-publish Docker Image sistem ke Docker Hub, sebuah layanan cloud yang memungkinkan kalian untuk membagikan aplikasi Docker kalian ke seluruh dunia. Output dari pekerjaan ini adalah file Docker kalian bisa dilihat secara public pada https://hub.docker.com/r/{Username}/storage-app.

## e
Berita tagline #YBBA (#YangBiruBiruAja) semakin populer, dan begitu juga sistem yang telah kalian buat untuk membantu Peb dalam rekrutmen pemain. Beberapa klub sepak bola lain mulai menunjukkan minat pada sistem tersebut dan permintaan penggunaan semakin bertambah. Untuk memastikan sistem kalian mampu menangani lonjakan permintaan ini, kalian memutuskan untuk menerapkan skala pada layanan menggunakan Docker Compose dengan instance sebanyak 5. Buat folder terpisah bernama Barcelona dan Napoli dan jalankan Docker Compose di sana.

<b>Catatan</b>: \
Cantumkan file hubdocker.txt yang berisi URL Docker Hub kalian (public).
Perhatikan port  pada masing-masing instance.

> penyelesaian 

dalam soal  a dan b , dibuat file bernama storage.c dengan kode program sebagai berikut.

```C
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#define MAX_LINE_LENGTH 20000
#define MAX_PLAYERS 20000

// Player* players = (Player*) malloc(MAX_PLAYERS * sizeof(Player));

typedef struct {
    char ID[10];
    char Name[50];
    int Age;
    char Photo[100];
    char Nationality[50];
    char Flag[100];
    int Overall;
    int Potential;
    char Club[50];
} Player;

Player players[MAX_PLAYERS];
int numPlayers = 0;

void downloadDataset();
void extractZip();
void printPotentialPlayers();
void parseCSV(const char *filename);


int main() {
    // Memperoleh data dengan mengunduh dataset
    downloadDataset();

    // Menunggu beberapa detik untuk memastikan unduhan selesai
    sleep(5);

    // Mengekstrak file ZIP
    extractZip();

    sleep(5);

    parseCSV("FIFA23_official_data.csv");
    printPotentialPlayers();

    return 0;
}

void downloadDataset() {
    // Command untuk mengunduh dataset menggunakan Kaggle CLI
    char download_command[1000];
    strcpy(download_command, "kaggle datasets download -d bryanb/fifa-player-stats-database");

    // Menjalankan perintah menggunakan fungsi system()
    system(download_command);
}


void extractZip() {
    // Command untuk mengekstrak file ZIP
    char extract_command[1000];
    strcpy(extract_command, "unzip fifa-player-stats-database.zip");

    // Menjalankan perintah menggunakan fungsi system()
    system(extract_command);
}

void parseCSV(const char *filename) {
    FILE *file = fopen(filename, "r");
    if (file == NULL) {
        printf("Failed to open file: %s\n", filename);
        exit(1);
    }

    char line[MAX_LINE_LENGTH];
    fgets(line, sizeof(line), file);  // Skip the header line

    while (fgets(line, sizeof(line), file)) {
        char *token;
        Player player;

        token = strtok(line, ",");
        strcpy(player.ID, token);

        token = strtok(NULL, ",");
        strcpy(player.Name, token);

        token = strtok(NULL, ",");
        player.Age = atoi(token);

        token = strtok(NULL, ",");
        strcpy(player.Photo, token);

        token = strtok(NULL, ",");
        strcpy(player.Nationality, token);

        token = strtok(NULL, ",");
        strcpy(player.Flag, token);

        token = strtok(NULL, ",");
        player.Overall = atoi(token);

        token = strtok(NULL, ",");
        player.Potential = atoi(token);

        token = strtok(NULL, ",");
        strcpy(player.Club, token);


        players[numPlayers++] = player;
    }

    fclose(file);
}

void printPotentialPlayers() {
    printf("Potential Players:\n");
    for (int i = 0; i < numPlayers; i++) {
        Player player = players[i];

        if (player.Age < 25 && player.Potential > 85 && strcmp(player.Club, "Manchester City") != 0) {
            printf("Name: %s\n", player.Name);
            printf("Club: %s\n", player.Club);
            printf("Age: %d\n", player.Age);
            printf("Potential: %d\n", player.Potential);
            printf("Photo URL: %s\n", player.Photo);
            printf("-----------------\n");
        }
    }
}
```

pada program tersebut, dilakukan unduh dataset pada fungsi `downloadDataset` yang akan mengunduh dataset dari kaggle. setelah dataset berhasil diunduh dalam bentuk zip, selanjutnya dilakukan unzip pada fungsi `extractZip`. dari kedua fungsi ini dihasilkan output sebagai berikut.

![](https://gitlab.com/tigoyoga/sisop-praktikum-modul-4-2023-bj-a09/uploads/01f205844cf9b9bb658e7253521547db/Screenshot_2023-06-03_203858.png)

lalu, pada fungsi `parseCSV` dan `printPotentialPlayers` dilakukan pembacaan data pada file csv yang diminta di soal lalu melakukan print pemain-pemain yang memenuhi kriteria pada soal.

berikut merupakan ouputnya .

![](https://gitlab.com/tigoyoga/sisop-praktikum-modul-4-2023-bj-a09/uploads/16e1189bd54a1c518896b11c3474c989/Screenshot_2023-06-03_203927.png)


selanjutnya, setelah Dockerfile berhasil dibuat dan diletakkan di dockerhub milik tigo. dilakukan pull dari dockerhub tersebut kemudian dilakukan run. pull dan run ini dilakukan di terminal. \
outputnya sama seperti sebelumnya, yaitu sebagai berikut :

![](https://gitlab.com/tigoyoga/sisop-praktikum-modul-4-2023-bj-a09/uploads/f7f24c232801d345ec2c588f8484f305/Screenshot_2023-06-03_204303.png)