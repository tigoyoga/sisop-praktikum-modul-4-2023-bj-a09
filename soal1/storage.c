#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#define MAX_LINE_LENGTH 20000
#define MAX_PLAYERS 20000

// Player* players = (Player*) malloc(MAX_PLAYERS * sizeof(Player));

typedef struct {
    char ID[10];
    char Name[50];
    int Age;
    char Photo[100];
    char Nationality[50];
    char Flag[100];
    int Overall;
    int Potential;
    char Club[50];
} Player;

Player players[MAX_PLAYERS];
int numPlayers = 0;

void downloadDataset();
void extractZip();
void printPotentialPlayers();
void parseCSV(const char *filename);


int main() {
    // Memperoleh data dengan mengunduh dataset
    downloadDataset();

    // Menunggu beberapa detik untuk memastikan unduhan selesai
    sleep(5);

    // Mengekstrak file ZIP
    extractZip();

    sleep(5);

    parseCSV("FIFA23_official_data.csv");
    printPotentialPlayers();

    return 0;
}

void downloadDataset() {
    // Command untuk mengunduh dataset menggunakan Kaggle CLI
    char download_command[1000];
    strcpy(download_command, "kaggle datasets download -d bryanb/fifa-player-stats-database");

    // Menjalankan perintah menggunakan fungsi system()
    system(download_command);
}


void extractZip() {
    // Command untuk mengekstrak file ZIP
    char extract_command[1000];
    strcpy(extract_command, "unzip fifa-player-stats-database.zip");

    // Menjalankan perintah menggunakan fungsi system()
    system(extract_command);
}

void parseCSV(const char *filename) {
    FILE *file = fopen(filename, "r");
    if (file == NULL) {
        printf("Failed to open file: %s\n", filename);
        exit(1);
    }

    char line[MAX_LINE_LENGTH];
    fgets(line, sizeof(line), file);  // Skip the header line

    while (fgets(line, sizeof(line), file)) {
        char *token;
        Player player;

        token = strtok(line, ",");
        strcpy(player.ID, token);

        token = strtok(NULL, ",");
        strcpy(player.Name, token);

        token = strtok(NULL, ",");
        player.Age = atoi(token);

        token = strtok(NULL, ",");
        strcpy(player.Photo, token);

        token = strtok(NULL, ",");
        strcpy(player.Nationality, token);

        token = strtok(NULL, ",");
        strcpy(player.Flag, token);

        token = strtok(NULL, ",");
        player.Overall = atoi(token);

        token = strtok(NULL, ",");
        player.Potential = atoi(token);

        token = strtok(NULL, ",");
        strcpy(player.Club, token);


        players[numPlayers++] = player;
    }

    fclose(file);
}

void printPotentialPlayers() {
    printf("Potential Players:\n");
    for (int i = 0; i < numPlayers; i++) {
        Player player = players[i];

        if (player.Age < 25 && player.Potential > 85 && strcmp(player.Club, "Manchester City") != 0) {
            printf("Name: %s\n", player.Name);
            printf("Club: %s\n", player.Club);
            printf("Age: %d\n", player.Age);
            printf("Potential: %d\n", player.Potential);
            printf("Photo URL: %s\n", player.Photo);
            printf("-----------------\n");
        }
    }
}